from tkinter import filedialog
from tkinter import Tk
from slidetoimage import overlaygenerator
import os
import sys
from time import sleep

root = Tk()
root.withdraw()
source = filedialog.askopenfilename(title="Source File",
                                    filetypes=(("pptx files","*.pptx"),))
destination = filedialog.askdirectory(title="Destination Directory")
destination = os.path.join(destination,'')
root.update()
root.destroy()
# start processing
generator = overlaygenerator.OverlayGenerator()
generator.generate(source,destination)
sys.exit(0)