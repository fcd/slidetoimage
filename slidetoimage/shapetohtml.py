from string import Template
from pptx.enum.dml import MSO_COLOR_TYPE
from pptx.enum.text import PP_PARAGRAPH_ALIGNMENT
from pptx.enum.shapes import PP_PLACEHOLDER_TYPE
import math


class ShapeToHtml:
    """ Take powerpoint shape and converts to html"""

    # constants for html templates
    html_template = Template('<html><body>$body<body></html>')
    table_template = Template('<center><table '
                              '>$body</table></center>')
    row_template = Template('<tr>$body</tr>')
    cell_template = Template('$body')
    td_template = Template('<td valign="top" colspan="$colspan" '
                           'align="$align">$body</td>')
    row_start = '<td valign="top" style="padding: 10px;">'
    row_end = '</td>'
    row_mid = Template('</span></td><td valign="top" style="padding: '
                       '10px;">$span')
    text_template = Template('$span$body</span>')
    div_start = Template('<div style="display:flex;">')
    div_end = Template('</div>')
    span_tag = Template('<span style="color:#$color;font-style:$style;'
                             'font-weight:$weight; font-family:arial; '
                        'font-size:$size%">')
    div_mid = Template('</span></div><div>$span')
    div_cell = Template('<div>$body</div>')
    base_font_size = 300
    base_ppt_font_size = 558800

    def convert_shape(self,shape):
        ''' converts the shape to block of html '''
        pass

    def shape_to_table(self, shape, complete=True):
        """
        reads text from a shape and adds to the html

        :param shape: the shape as read by pptx
        :param complete: if true html is a complete page.  If false it is a
        snippet that should be embedded into a page.
        :return: html table
        """

        table = ''
        # there maybe no text frame
        if hasattr(shape, 'text_frame'):
            for paragraph in shape.text_frame.paragraphs:
                row = self.row_start
                for idx, run in enumerate(paragraph.runs):
                    span = self.span_generator(run)

                    tab_replacer = self.row_mid.substitute(span=span)
                    text = run.text
                    text = self.break_adder(text, shape.text)
                    text = text.replace('\t', tab_replacer)
                    text = self.text_template.substitute(body=text,
                                                         span=span)
                    row += self.cell_template.substitute(body=text)
                row += self.row_end
                table += self.row_template.substitute(body=row)
            table = self.table_template.substitute(body=table)
            if complete:
                table = self.html_template.substitute(body=table)
            return table
        else:
            return self.html_template.substitute(body="")

    def shape_to_div(self, shape, complete=True):
        """
        generates div version
        :param shape:
        :param complete:
        :return:
        """
        if hasattr(shape, 'text_frame'):
            body = ""
            for paragraph in shape.text_frame.paragraphs:
                row = self.div_start.substitute()
                for run in paragraph.runs:
                    span = self.span_generator(run)
                    tab_replaceer = self.div_mid.substitute(span=span)
                    text = run.text
                    text = self.break_adder(text, shape.text)
                    text = text.replace('\t', tab_replaceer)
                    text = self.text_template.substitute(body=text, span=span)
                    row += self.div_cell.substitute(body=text)
                row += self.div_end.substitute()
                body += row
            if complete:
                return self.html_template.substitute(body=body)
        else:
            return self.html_template.substitute(body="")

    def shape_to_table_adv(self, shape, complete=True):
        if hasattr(shape, 'text_frame'):
            table = list()
            max_cell_count = 1
            for paragraph in shape.text_frame.paragraphs:
                row = list()
                text = paragraph.text.replace('\x0b','<br />')
                text_blocks = text.split('\t')
                for block in text_blocks:
                    row.append(block)
                if len(row) > max_cell_count:
                    max_cell_count = len(row)
                table.append(row)
            table_inner_html = ""
            for row, paragraph in zip(table, shape.text_frame.paragraphs):
                row_html = ""
                if paragraph.alignment == PP_PARAGRAPH_ALIGNMENT.CENTER:
                    # haven't found all the places wehre alignment is set
                    align = "center"
                elif paragraph.alignment == None and \
                        shape.is_placeholder and \
                        shape.placeholder_format.type == \
                        PP_PLACEHOLDER_TYPE.CENTER_TITLE:
                    align = "center"
                else:
                    align = "left"
                if len(row) != 0:
                    if len(row) < max_cell_count:
                        # then the column span of the first item needs to be the
                        # difference
                        col_span = max_cell_count - len(row) + 1
                        body = row.pop(0)
                        body += "&nbsp;"
                        row_html += self.td_template.substitute(
                            body=body,
                            colspan=col_span,
                            align=align
                        )
                    for cell in row:
                        # add the remaining
                        body = cell
                        body += "&nbsp;"
                        row_html += self.td_template.substitute(body=body,
                                                                colspan=1,
                                                                align=align)
                    for run in paragraph.runs:
                        # basically this is search and replace
                        run_style = dict()
                        run_style['italic'] = run.font.italic
                        run_style['bold'] = run.font.bold
                        if run.font.color.type == MSO_COLOR_TYPE.RGB:
                            run_style['color'] = run.font.color.rgb
                        else:
                            run_style['color'] = "FFFFFF"
                        # should eventually replace with something smarter
                        if run.font.size is not None:
                            #this is kinda funky
                            run_style['font_size'] = math.floor(
                                self.base_font_size *
                                run.font.size /
                                self.base_ppt_font_size
                            )
                        else:
                            run_style['font_size'] = 300
                        # split into different items based on the tab
                        text_blocks = run.text.split('\t')
                        for block in text_blocks:
                            if block != " " and block != "&nbsp;" and \
                                    block != "":
                                # for the moment ignore : as they cause issues
                                # only replace non-empty none space blocks
                                run_style['text'] = block
                                # want to start replacement from the last
                                # span tag
                                start_at = row_html.rfind('</span>')
                                new_html = row_html[start_at+1:].replace(
                                    block,
                                    self.cell_to_html(run_style),
                                    1
                                )
                                row_html = row_html[:start_at+1] + new_html
                table_inner_html += self.row_template.substitute(body=row_html)
            table = self.table_template.substitute(body=table_inner_html)
            # reencode table
            return table.encode('ascii','xmlcharrefreplace').decode('ascii','')
        return ""

    def cell_to_html(self,cell):
        if cell['italic']:
            style = "italic"
        else:
            style = "normal"
        if cell['bold']:
            weight = "bold"
        else:
            weight = "normal"
        span_tag = self.span_tag.substitute(color=cell['color'],
                                            style=style,
                                            weight=weight,
                                            size=cell['font_size'])
        return self.text_template.substitute(span=span_tag, body=cell['text'])

    def span_generator(self, run):
        """
        generates the span tags used to set the font for a ppt run
        :param run: a run object
        :return: the <span> tag
        """
        if run.font.italic:
            style = "italic"
        else:
            style = "normal"
        if run.font.bold:
            weight = "bold"
        else:
            weight = "normal"
        # if the color type isn't RGB assume it's white
        if run.font.color.type == MSO_COLOR_TYPE.RGB:
            color = run.font.color.rgb
        else:
            color = "FFFFFF"
        return self.span_tag.substitute(color = color,
                                        style = style,
                                        weight = weight)

    def break_adder(self, text, shape_text):
        """
        adds line breaks as required (since they're not in runs)
        :param text: the text from the run
        :param shape_text: the text from the shape
        :return: text with line breaks added
        """
        text_location = shape_text.find(text)
        # apparently the run does not have the newline
        potential_location = len(text) + text_location
        if (len(shape_text) - 1 > potential_location) and \
                (shape_text[potential_location] == '\x0b'):
            text += "<br />"
        return text

    def shape_top(self,shape):
        return shape.top

    def multi_shapes_to_html(self, shapes):
        '''
        Converts multiple shapes from a ppt shapes collection to html
        :param shape_list: pptx shapes collection containing the shapes to
        convert
        :return: html version of all the shapes
        '''
        # first convert collection to a list
        shape_list = list()
        for shape in shapes:
            shape_list.append(shape)
        # next sort it based on top
        shape_list.sort(key=self.shape_top)
        # now run through the shapes
        html = ""
        for shape in shape_list:
            html += self.shape_to_table_adv(shape)
        return self.html_template.substitute(body=html)
