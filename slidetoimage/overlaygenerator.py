from pptx import Presentation
from slidetoimage.shapetohtml import ShapeToHtml
import imgkit
from PIL import Image, ImageDraw, ImageTk
from tkinter import Tk, HORIZONTAL, Label, StringVar, Canvas, NW
from tkinter.ttk import Progressbar
import platform


class OverlayGenerator:
    '''OverlayGenerator take the text sources and uses it to create an Ovleray'''
    shape_to_html = ShapeToHtml()
    imgkit_width = 1700
    imgkit_options = {'transparent': None, 'width': imgkit_width, 'quiet':
        None}
    temp_image_path = 'temp.png'
    img_height = 1080
    img_width = 1920
    img_x_offset = int((img_width - imgkit_width)/2)
    # distance to offset from bottom (due to TV overscan)
    bottom_offset = 0
    # amount to pad the bounding box of the text
    padding = 20
    # text background
    text_background = (0,0,0,100)
    # generate(self, target) creates the image saving to the "target" location
    #  on the file system
    def generate(self, ppt, target):
        """
        generates the image set
        :param ppt: the power point presentation to process
        :param target: the location to put the images
        :return: None
        """
        prs = Presentation(ppt)
        tk = Tk()
        tk.title('Processing...')
        label_text = StringVar()
        label_text.set("Now Processing %s of %s" % ('0', str(len(
            prs.slides))))
        label = Label(tk, textvariable=label_text)
        label.pack()
        progress = Progressbar(tk, orient=HORIZONTAL, length=len(prs.slides),
                               mode='determinate')
        progress.pack()
        # let try making a place to display the image
        #canvas = Canvas(tk, width=720, height=480)
        #canvas.pack()
        #disp_img = canvas.create_image(0,0)
        img_label = Label(tk)
        img_label.pack()

        for idx, slide in enumerate(prs.slides):
            progress['value'] = idx+1
            label_text.set("Now Processing %s of %s" % (str(idx+1), str(len(
                prs.slides))))
            tk.update()
            image_name = target+'slide'+str(idx)+'.png'
            image = self.gen_image(slide)
            # update the canvas
            tk_img = ImageTk.PhotoImage(image.resize((720,480)).convert('RGB'))
            img_label.configure(image=tk_img)

            image.save(image_name,'png')
        tk.destroy()

    def gen_image(self, slide):
        """
        generates on image from a slide
        :param slide: the slide object
        :return: image
        """
        image = Image.new('RGBA',(self.img_width, self.img_height),(0,0,0,0))
        drawer = ImageDraw.Draw(image)
        # this is the initial processing of a single shape
        if len(slide.shapes) > 0:
            # note the image generator doesn't support the "flex" display
            # apparently so it's not used.
            html = self.shape_to_html.multi_shapes_to_html(slide.shapes)
            # it appears that the alpha channel is lost if it's written to
            # binary
            if platform.system() == "Windows":
                imgkit_config = imgkit.config(wkhtmltoimage='C:/Program Files/wkhtmltopdf/bin/wkhtmltoimage.exe')
                imgkit.from_string(html, self.temp_image_path,
                               options=self.imgkit_options, config=imgkit_config)
            else:
                imgkit.from_string(html, self.temp_image_path,
                                   self.imgkit_options)
            # load the image with PILLOW
            overlay = Image.open(self.temp_image_path)
            # find the bounding box (used for x box drawing)
            text_box = overlay.getbbox()
            # calculate the rectange corners
            if text_box != None:
                # if the image is empty text box is None so no reason to include
                x0 = text_box[0]-self.padding+self.img_x_offset
                x1 = text_box[2]+self.padding+self.img_x_offset
                y0 = self.img_height-overlay.size[1]-self.bottom_offset-self.padding
                y1 = self.img_height-self.bottom_offset+self.padding
                # draw the rectangles
                drawer.rectangle([(x0, y0), (x1, y1)], self.text_background)
                # put the text on
                image.alpha_composite(overlay, (self.img_x_offset, y0))
        return image
    # addTextBlock(self, text) adds the "next line" of text to generator

