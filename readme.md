# Steps to install and run the overlay generator:
1. Download [WKhtmlTOpdf](https://wkhtmltopdf.org/downloads.html) for your platform [Windows](https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-rc/wkhtmltox-0.12.6-0.20200605.30.rc.faa06fa.msvc2015-win64.exe)
[macOS](https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-rc/wkhtmltox-0.12.6-0.20200605.30.rc.faa06fa.macos-cocoa.pkg)
2. Install WKhtmlTOpdf
3. Download [GenerateOverlay](https://bitbucket.org/fcd/slidetoimage/downloads/) 
[Windows](https://bitbucket.org/fcd/slidetoimage/downloads/GenerateOverlay.exe)
[macOS](https://bitbucket.org/fcd/slidetoimage/downloads/GenerateOverlay)  Note there is no Linux build at this time. 
4. Run GenerateOverlay.  
5. In the first popup select the powerpoint file you with to generate from
6. In the second popup select the destination directory for it.


Note a guide on how the overlay generator interprets PowerPoint can be found [here](https://bitbucket.org/fcd/slidetoimage/downloads/Slide_Creation_Guide.pptx)